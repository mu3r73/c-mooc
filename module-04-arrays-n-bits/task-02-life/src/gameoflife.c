#include "gameoflife.h"
#include <stdio.h>
#include <stdlib.h>

/* Exercise a: Allocates needed memory for the field structure and
 * the actual game field. 'xsize' and 'ysize' indicate the horizontal and
 * vertical dimensions of the field.
 *
 * Returns: pointer to the Field structure allocated by this function.
 */
Field *createField(unsigned int xsize, unsigned int ysize) {
  Field *field = (Field *)malloc(sizeof(Field));
  if (field == NULL) {
    free(field);
    return NULL;
  }

  field->cells = (State **)malloc(ysize * sizeof(State *));
  if (field->cells == NULL) {
    free(field->cells);
    free(field);
    return NULL;
  }

  for (unsigned int i = 0; i < ysize; i++) {
    field->cells[i] = (State *)malloc(xsize * sizeof(State));
    if (field->cells[i] == NULL) {
      for (unsigned int j = 0; j < i; j++) {
        free(field->cells[i]);
      }
      free(field->cells);
      free(field);
      return NULL;
    }
  }

  for (unsigned int y = 0; y < ysize; y++) {
    for (unsigned int x = 0; x < xsize; x++) {
      field->cells[y][x] = DEAD;
    }
  }
  field->xsize = xsize;
  field->ysize = ysize;

  return field;
}

/* Free memory allocated for field <f>.
 */
void releaseField(Field *f) {
  if (f == NULL) {
    return;
  }
  for (unsigned int i = 0; i < f->ysize; i++) {
    free(f->cells[i]);
  }
  free(f->cells);
  free(f);
}

/* Exercise b: Initialize game field by setting exactly <n> cells into
 * ALIVE state in the game field <f>.
 */
void initialize_random_cell(Field *f) {
  unsigned int x = rand() % f->xsize;
  unsigned int y = rand() % f->ysize;
  while (f->cells[y][x] == ALIVE) {
    x = rand() % f->xsize;
    y = rand() % f->ysize;
  }
  f->cells[y][x] = ALIVE;
}

void initField(Field *f, unsigned int n) {
  unsigned int done = 0;
  while (done++ < n) {
    initialize_random_cell(f);
  }
}

/* Exercise c: Output the current state of field <f>.
 */
void printField(const Field *f) {
  for (unsigned int y = 0; y < f->ysize; y++) {
    for (unsigned int x = 0; x < f->xsize; x++) {
      if (f->cells[y][x] == ALIVE) {
        printf("%c", '*');
      } else {
        printf("%c", '.');
      }
    }
    printf("\n");
  }
}

/* Exercise d: Advance field <f> by one generation.
 */
unsigned int min_neigh(unsigned int coord) {
  if (coord > 0) {
    return coord - 1;
  } else {
    return coord;
  }
}

unsigned int max_neigh(unsigned int coord, unsigned int maxcoord) {
  if (coord < maxcoord - 1) {
    return coord + 1;
  } else {
    return coord;
  }
}

int count_alive_neighbours(const Field *f, unsigned int x0, unsigned int y0) {
  int count = 0;
  for (unsigned int y = min_neigh(y0); y <= max_neigh(y0, f->ysize); y++) {
    for (unsigned int x = min_neigh(x0); x <= max_neigh(x0, f->xsize); x++) {
      if ((x != x0) || (y != y0)) {
        if (f->cells[y][x] == ALIVE) {
          count++;
        }
      }
    }
  }
  return count;
}

void tick(Field *f) {
  Field *newf = createField(f->xsize, f->ysize);

  for (unsigned int y = 0; y < f->ysize; y++) {
    for (unsigned int x = 0; x < f->xsize; x++) {
      int alive_neighbours = count_alive_neighbours(f, x, y);
      if (f->cells[y][x] == ALIVE) {
        if ((alive_neighbours < 2) || (alive_neighbours > 3)) {
          newf->cells[y][x] = DEAD;
        } else {
          newf->cells[y][x] = ALIVE;
        }
      } else { // it's dead, Jim
        if (alive_neighbours == 3) {
          newf->cells[y][x] = ALIVE;
        } else {
          newf->cells[y][x] = f->cells[y][x];
        }
      }
    }
  }

  for (unsigned int y = 0; y < f->ysize; y++) {
    for (unsigned int x = 0; x < f->xsize; x++) {
      f->cells[y][x] = newf->cells[y][x];
    }
  }

  releaseField(newf);
}

/*
Any live cell with fewer than two live neighbours dies
Any live cell with two or three live neighbours lives on to the next generation.
Any live cell with more than three live neighbours dies
Any dead cell with exactly three live neighbours becomes a live cell.
(diagonal cells are considered neighbors, i.e., each cell (that is not on the edge of the field) has 8 neighbors)
*/
