#include "ipheader.h"
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Parses the given buffer into an IP header structure.
 *
 * Parameters:
 * ip: pointer to the IP header structure that will be filled based
 *      on the data in the buffer
 * buffer: buffer of 20 bytes that contain the IP header. */
int get_version(const unsigned char *buffer) {
  return (buffer[0] & 0xF0) >> 4;
}

int get_ihl(const unsigned char *buffer) {
  return (buffer[0] & 0x0F) * 4;
}

int get_dscp(const unsigned char *buffer) {
  return (buffer[1] & 0xFC) >> 2;
}

int get_ecn(const unsigned char *buffer) {
  return buffer[1] & 0x03;
}

unsigned short get_length(const unsigned char *buffer) {
  return (buffer[2] << 8) + buffer[3];
}

unsigned short get_id(const unsigned char *buffer) {
  return (buffer[4] << 8) + buffer[5];
}

int get_flags(const unsigned char *buffer) {
  return (buffer[6] & 0xE0) >> 5;
}

int get_fragment_offset(const unsigned char *buffer) {
  return ((buffer[6] & 0x1F) << 8) + buffer[7];
}

int get_time_to_live(const unsigned char *buffer) {
  return buffer[8];
}

int get_protocol(const unsigned char *buffer) {
  return buffer[9];
}

unsigned short get_header_checksum(const unsigned char *buffer) {
  return (buffer[10] << 8) + buffer[11];
}

void get_source_ip(unsigned char *source_ip, const unsigned char *buffer) {
  source_ip[0] = buffer[12];
  source_ip[1] = buffer[13];
  source_ip[2] = buffer[14];
  source_ip[3] = buffer[15];
}

void get_dest_ip(unsigned char *dest_ip, const unsigned char *buffer) {
  dest_ip[0] = buffer[16];
  dest_ip[1] = buffer[17];
  dest_ip[2] = buffer[18];
  dest_ip[3] = buffer[19];
}

void parseIp(struct ipHeader *ip, const void *buffer) {
  ip->version = get_version((unsigned char *)buffer);
  ip->ihl = get_ihl((unsigned char *)buffer);
  ip->dscp = get_dscp((unsigned char *)buffer);
  ip->ecn = get_ecn((unsigned char *)buffer);
  ip->length = get_length((unsigned char *)buffer);
  ip->identification = get_id((unsigned char *)buffer);
  ip->flags = get_flags((unsigned char *)buffer);
  ip->fragment_offset = get_fragment_offset((unsigned char *)buffer);
  ip->time_to_live = get_time_to_live((unsigned char *)buffer);
  ip->protocol = get_protocol((unsigned char *)buffer);
  ip->header_checksum = get_header_checksum((unsigned char *)buffer);
  get_source_ip(ip->source_ip, (unsigned char *)buffer);
  get_dest_ip(ip->destination_ip, (unsigned char *)buffer);
}

/* Builds a 20-byte byte stream based on the given IP header structure
 *
 * Parameters:
 * buffer: pointer to the 20-byte buffer to which the header is constructed
 * ip: IP header structure that will be packed to the buffer */
unsigned char get_byte_0(const struct ipHeader *ip) {
  return (ip->version << 4) + (ip->ihl / 4);
}

unsigned char get_byte_1(const struct ipHeader *ip) {
  return (ip->dscp << 2) + ip->ecn;
}

unsigned char get_byte_2(const struct ipHeader *ip) {
  return (ip->length & 0xFF00) >> 8;
}

unsigned char get_byte_3(const struct ipHeader *ip) {
  return ip->length & 0x00FF;
}

unsigned char get_byte_4(const struct ipHeader *ip) {
  return (ip->identification & 0xFF00) >> 8;
}

unsigned char get_byte_5(const struct ipHeader *ip) {
  return ip->identification & 0x00FF;
}

unsigned char get_byte_6(const struct ipHeader *ip) {
  return (ip->flags << 5) + ((ip->fragment_offset & 0x1F00) >> 8);
}

unsigned char get_byte_7(const struct ipHeader *ip) {
  return ip->fragment_offset & 0x00FF;
}

unsigned char get_byte_10(const struct ipHeader *ip) {
  return (ip->header_checksum & 0xFF00) >> 8;
}

unsigned char get_byte_11(const struct ipHeader *ip) {
  return ip->header_checksum & 0x00FF;
}

void sendIp(void *buffer, const struct ipHeader *ip) {
  unsigned char *b = (unsigned char *)buffer;
  b[0] = get_byte_0(ip);
  b[1] = get_byte_1(ip);
  b[2] = get_byte_2(ip);
  b[3] = get_byte_3(ip);
  b[4] = get_byte_4(ip);
  b[5] = get_byte_5(ip);
  b[6] = get_byte_6(ip);
  b[7] = get_byte_7(ip);
  b[8] = ip->time_to_live;
  b[9] = ip->protocol;
  b[10] = get_byte_10(ip);
  b[11] = get_byte_11(ip);
  b[12] = ip->source_ip[0];
  b[13] = ip->source_ip[1];
  b[14] = ip->source_ip[2];
  b[15] = ip->source_ip[3];
  b[16] = ip->destination_ip[0];
  b[17] = ip->destination_ip[1];
  b[18] = ip->destination_ip[2];
  b[19] = ip->destination_ip[3];
}

/* Prints the given IP header structure */
void printIp(const struct ipHeader *ip) {
  /* Note: ntohs below is for converting numbers from network byte order
   to host byte order. You can ignore them for now
   To be discussed further in Network Programming course... */
  printf("version: %d   ihl: %d   dscp: %d   ecn: %d\n", ip->version, ip->ihl,
         ip->dscp, ip->ecn);
  printf("length: %d   id: %d   flags: %d   offset: %d\n", ntohs(ip->length),
         ntohs(ip->identification), ip->flags, ip->fragment_offset);
  printf("time to live: %d   protocol: %d   checksum: 0x%04x\n",
         ip->time_to_live, ip->protocol, ntohs(ip->header_checksum));
  printf("source ip: %d.%d.%d.%d\n", ip->source_ip[0], ip->source_ip[1],
         ip->source_ip[2], ip->source_ip[3]);
  printf("destination ip: %d.%d.%d.%d\n", ip->destination_ip[0],
         ip->destination_ip[1], ip->destination_ip[2], ip->destination_ip[3]);
}

/* Shows hexdump of given data buffer */
void hexdump(const void *buffer, unsigned int length) {
  const unsigned char *cbuf = (const unsigned char *)buffer;
  unsigned int i;
  for (i = 0; i < length;) {
    printf("%02x ", cbuf[i]);
    i++;
    if (!(i % 8))
      printf("\n");
  }
}
