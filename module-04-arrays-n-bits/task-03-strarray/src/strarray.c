#include "strarray.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Exercise a: Initializes the string array to contain the initial
 * NULL pointer, but nothing else.
 * Returns: pointer to the array of strings that has one element
 *      (that contains NULL)
 */
char **init_array(void) {
  char **res = (char **)malloc(sizeof(char *));
  res[0] = NULL;
  return res;
}

/* Releases the memory used by the strings.
 */
void free_strings(char **array) {
  if (array == NULL) {
    return;
  }
  for (unsigned int i = 0; ; i++) {
    if (array[i] == NULL) {
      break;
    }
    free(array[i]);
  }
  free(array);
}

/* Exercise b: Add <string> to the end of array <array>.
 * Returns: pointer to the array after the string has been added.
 */
unsigned int size(char **array) {
  unsigned int count = 0;
  if (array != NULL) {
    for (unsigned int i = 0; ; i++) {
      if (array[i] == NULL) {
        break;
      }
      count++;
    }
  }
  return count;
}

char **add_string(char **array, const char *string) {
  unsigned int alen = size(array);
  char **newa = (char **)realloc(array, (alen + 2) * sizeof(char *));
  if (newa == NULL) {
    free(newa);
    return array;
  }

  unsigned int len = strlen(string);
  newa[alen] = (char *)malloc((len + 1) * sizeof(char));
  if (newa[alen] == NULL) {
    for (unsigned int i = 0; i <= alen; i++) {
      free(newa[i]);
    }
    free(newa);
    return array;
  }
  strncpy(newa[alen], string, len + 1);

  alen++;
  newa[alen] = NULL;

  return newa;
}

/* Exercise c: Convert letters of all strings in <array> to lower case.
 */
void make_lower(char **array) {
  if (array == NULL) {
    return;
  }
  for (unsigned int j = 0; ; j++) {
    if (array[j] == NULL) {
      break;
    }
    for (unsigned int i = 0; i < strlen(array[j]); i++) {
      array[j][i] = tolower(array[j][i]);
    }
  }
}

/* Exercise d: reorder strings in <array> to lexicographical order */
unsigned int pos_min_from(char **array, unsigned int start) {
  int pos = start;
  for (unsigned int i = start + 1; ; i++) {
    if (array[i] == NULL) {
      break;
    }
    if (strcmp(array[i], array[pos]) < 0) {
      pos = i;
    }
  }
  return pos;
}

void swap(char **a, char **b) {
  char *aux;
  aux = *a;
  *a = *b;
  *b = aux;
}

void sort_strings(char **array) {
  for (unsigned int i = 0; ; i++) {
    if (array[i] == NULL) {
      break;
    }
    unsigned int pos_min = pos_min_from(array, i);
    if (pos_min > i) {
      swap(&(array[i]), &(array[pos_min]));
    }
  }
}

/* You can use this function to check what your array looks like.
 */
void print_strings(char **array) {
  if (!array)
    return;
  while (*array) {
    printf("%s  ", *array);
    array++;
  }
  printf("\n");
}
