#include "source.h"
#include <string.h>

void insert_str(char *str, const char *src) {
  for (unsigned int i = 0; ; i++) {
    str[i] = src[i];
    if (src[i] == '\0') {
      break;
    }
  }
}

/* Korsoraattori
 */
void korsoroi(char *dest, const char *src) {
  int word = 0;
  int posd = 0;
  for (unsigned int i = 0; ; i++) {
    if (strstr(src + i, "ks") == src + i) {
      dest[posd++] = 'x';
      i++;
    } else if (strstr(src + i, "ts") == src + i) {
      dest[posd++] = 'z';
      i++;
    } else if (src[i] == ' ') {
      dest[posd++] = ' ';
      word++;
      if (word % 3 == 0) {
        insert_str(dest + posd, "niinku ");
        posd += 7;
      } else if (word % 4 == 0) {
        insert_str(dest + posd, "totanoin ");
        posd += 9;
      }
    } else {
      dest[posd++] = src[i];
      if (src[i] == '\0') {
        break;
      }
    }
  }
}

/*
Every instance of "ks" should be changed to "x".
Every instance of "ts" should be changed to "z".
After every third word in the original string there should be additional word "niinku" in the destination string.
After every fourth word in the original string there should be additional word "totanoin" in the destination string.
*/
