#include "source.h"
#include <ctype.h>
#include <string.h>

/* Count Alpha
 * Count number of alphabetic characters in the given string <str>,
 * and return the count
 */
int count_alpha(const char *str) {
  int count = 0;
  for (unsigned int i = 0; ; i++) {
    if (isalpha(str[i]) != 0) {
      count++;
    } else if (str[i] == '\0') {
      break;
    }
  }
  return count;
}

/* Count Substring
 * Count number of occurances of substring <sub> in string <str>,
 * and return the count.
 */
int count_substr(const char *str, const char *sub) {
  int num = 0;
  char *pos = strstr((char *)str, sub);
  while (pos != NULL) {
    num++;
    pos = strstr(pos + 1, sub);
  }
  return num;
}
