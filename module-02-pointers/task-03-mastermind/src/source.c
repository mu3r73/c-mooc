#include "source.h"
#include <stdio.h>

#ifndef MAWKKE
// longer solution by Pasi

int contains(const int *array, unsigned int len, int elem) {
  int res = 0;
  for (unsigned int i = 0; i < len; i++) {
    if (array[i] == elem) {
      res = 1;
      break;
    }
  }
  return res;
}

/* 03-mastermind
 */
void mastermind(const int *solution, const int *guess, char *result,
                unsigned int len) {
  for (unsigned int i = 0; i < len; i++) {
    if (guess[i] == solution[i]) {
      result[i] = '+';
    } else if (contains(solution, len, guess[i]) == 1) {
      result[i] = '*';
    } else {
      result[i] = '-';
    }
  }
}
#endif
