#include "source.h"
#include <stdio.h>

/* Print string */
/* Parameters:
 * s: string to be printed */
void es_print(const char *s) {
  for (unsigned int i = 0; ; i++) {
    if (s[i] == '#') {
      break;
    }
    printf("%c", s[i]);
  }
}

/* String length */
/* Parameters:
 * s: string to be evaluated
 * Returns: length of the string */
unsigned int es_length(const char *s) {
  int len = 0;
  for (unsigned int i = 0; ; i++) {
    if (s[i] == '#') {
      break;
    }
    len++;
  }
  return len;
}

/* String copy */
/* Parameters:
 * dst: buffer to which the new string is copied
 * src: original string
 * Returns: Number of characters copied
 */
int es_copy(char *dst, const char *src) {
  int len = 0;
  for (unsigned int i = 0; ; i++) {
    if (src[i] == '#') {
      break;
    }
    dst[i] = src[i];
    len++;
  }
  dst[len] = '#';
  return len;
}

/* String tokenizer */
/* Parameters:
 * s: string to be processed
 * c: character to be replaced by '#'
 * Returns: pointer to the character following the replaced character,
 *          NULL if end of string reached */
char *es_token(char *s, char c) {
  char *pos = NULL;
  for (unsigned int i = 0; ; i++) {
    if (s[i] == '#') {
      break;
    }
    if (s[i] == c) {
      s[i] = '#';
      pos = s + i + 1;
      break;
    }
  }
  return pos;
}
