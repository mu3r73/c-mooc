#include "source.h"

int pos_of_min_in_slice(int *array, int start, int end) {
  int pos = start;
  for (int i = start + 1; i < end; i++) {
    if (array[i] < array[pos]) {
      pos = i;
    }
  }
  return pos;
}

void swap(int *i, int *j) {
  int tmp;
  tmp = *i;
  *i = *j;
  *j = tmp;
}

/* Selection sort */
/* Parameters:
 * start: start of an array
 * size: length of an array
 */
void sort(int *start, int size) {
  for (int i = 0; i < size; i++) {
    int pos_min = pos_of_min_in_slice(start, i, size);
    if (pos_min > i) {
      swap(start + i, start + pos_min);
    }
  }
}
