#include "source.h"
#include <stdio.h>

/* Print string array, last element is NULL */
/* Parameters:
 * array: string array to be printed, each string on own line */
void print_strarray(char *array[]) {
  for (unsigned int i = 0; ; i++) {
    if (array[i] == NULL) {
      break;
    }
    printf("%s\n", array[i]);
  }
}

/* Put strings from string separated by space to a string array */
/* Parameters:
 * string: string to be cut into parts and placed into the array,
   remember to add ending zeros '\0' for strings and NULL to the end of the
 whole array!
 * arr: ready-made array that the strings will be put into */
void str_to_strarray(char *string, char **arr) {
  unsigned int posx = 0;
  unsigned int posy = 0;
  for (unsigned char i = 0; ; i++) {
    if (string[i] == ' ') {
      arr[posx++][posy] = '\0';
      posy = 0;
    } else {
      arr[posx][posy++] = string[i];
      if (string[i] == '\0') {
        posx++;
        break;
      }
    }
  }
  arr[posx] = NULL;
}
