#include "source.h"
#include <stdio.h>
#include <stdlib.h>

/* Dynamic Array Reader */
/* Parameters:
 * n: Number of values to be read
 *
 * Returns: pointer to the dynamically allocated array
 */
int *dyn_reader(unsigned int n) {
  int *res;
  res = (int *)malloc(n * sizeof(int));
  if (res == NULL) {
    free(res);
    return res;
  }

  unsigned int num_read = 0;
  int num, ret_code;
  while (num_read < n) {
    ret_code = scanf("%d", &num);
    if (ret_code > 0) {
      res[num_read++] = num;
    }
  }

  return res;
}

/* Add to array */
/* Parameters:
 * arr: Existing array of integers
 * num: number of integers in the array before the call
 * newval: new value to be added
 *
 * Returns: pointer to the allocated array
 */
int *add_to_array(int *arr, unsigned int num, int newval) {
  int *res = (int *)realloc(arr, (num + 1) * sizeof(int));
  if (res == NULL) {
    free(res);
    return arr;
  } else {
    res[num] = newval;
    return res;
  }
}
