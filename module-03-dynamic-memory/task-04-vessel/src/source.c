#include "source.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

unsigned int min(unsigned int a, unsigned int b) {
  if (a <= b) {
    return a;
  } else {
    return b;
  }
}

struct vessel create_vessel(const char *p_name, double p_length, double p_depth,
                            struct cargo p_crg) {
  struct vessel res;

  res.name = (char *)malloc(31 * sizeof(char));
  if (res.name != NULL) {
    unsigned int len = min(30, strlen(p_name));
    strncpy(res.name, p_name, len);
    res.name[len] = '\0';
  }

  res.length = p_length;
  res.depth = p_depth;
  res.crg = p_crg;

  return res;
}

void print_vessel(const struct vessel *ship) {
  printf("%s\n%.1f\n%.1f\n%s\n%d\n%.1f\n", ship->name, ship->length, ship->depth, ship->crg.title, ship->crg.quantity, ship->crg.weight);
}
