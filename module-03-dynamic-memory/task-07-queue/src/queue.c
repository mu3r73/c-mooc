#include "queue.h"
#include "queuepriv.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const unsigned int max_student_id_len = 6;

Queue *Queue_init(void) {
  Queue *q = calloc(1, sizeof(Queue));
  return q;
}

int is_valid(const char *id) {
  if (strlen(id) > max_student_id_len) {
    return 0;
  } else {
    return 1;
  }
}

int is_empty(Queue *q) {
  if ((q->first == NULL) || (q->last == NULL)) {
    return 1;
  } else {
    return 0;
  }
}

int Queue_enqueue(Queue *q, const char *id, const char *name) {
  if (!is_valid(id)) {
    return 0;
  }
  struct student *stdnt = (struct student *)malloc(sizeof(struct student));
  if (stdnt == NULL) {
    free(stdnt);
    return 0;
  }
  unsigned int len = strlen(id);
  strncpy(stdnt->id, id, len);
  stdnt->id[len] = '\0';

  len = strlen(name);
  stdnt->name = (char *)malloc(len * sizeof(char));
  if (stdnt->name != NULL) {
    strncpy(stdnt->name, name, len);
    stdnt->name[len] = '\0';
  }

  stdnt->next = NULL;

  if (is_empty(q)) {
    q->first = stdnt;
  } else {
    q->last->next = stdnt;
  }
  q->last = stdnt;

  return 1;
}

char *Queue_firstID(Queue *q) {
  if (q && q->first)
    return q->first->id;
  else
    return NULL;
}

char *Queue_firstName(Queue *q) {
  if (q && q->first)
    return q->first->name;
  else
    return NULL;
}

int Queue_dequeue(Queue *q) {
  if (is_empty(q)) {
    return 0;
  }

  struct student *aux = q->first;
  q->first =  q->first->next;
  free(aux->name);
  free(aux);

  if (is_empty(q)) {
    q->first = NULL;
    q->last = NULL;
  }

  return 1;
}

int Queue_drop(Queue *q, const char *id) {
  struct student *aux = q->first;
  struct student *prev = NULL;
  while (aux != NULL) {
    if (*(aux->id) == *id) {
      if (prev != NULL) {
        prev->next = aux->next;
      } else {
        q->first = aux->next;
      }
      if (aux->next == NULL) {
        q->last = prev;
      }
      free(aux->name);
      free(aux);
      return 1;
    }
    prev = aux;
    aux = aux->next;
  }
  return 0;
}

void Queue_delete(Queue *q) {
  if (q) {
    while (Queue_dequeue(q))
      ;
    free(q);
  }
}
