#include "ships.h"
#include <stdio.h>
#include <stdlib.h>

const unsigned int xsize = 10;
const unsigned int ysize = 10;
const unsigned int shiplen = 3;

/* implement these functions */

unsigned int get_random_coord() {
  return rand() % xsize;
}

int get_random_dir() {
  return rand() % 2;
}

void set_ship() {
  unsigned int x = get_random_coord();
  unsigned int y = get_random_coord();
  int dir = get_random_dir();
  while (place_ship(x, y, dir) == 0) {
    x = get_random_coord();
    y = get_random_coord();
    dir = get_random_dir();
  }
}

/* Task a: Place <num> ships on the game map.
 */
void set_ships(unsigned int num) {
  for (unsigned int i = 0; i < num; i++) {
    set_ship();
  }
}

char get_char_to_print(unsigned int x, unsigned int y) {
  if (is_visible(x, y) == 1) {
    return is_ship(x, y);
  } else {
    return '?';
  }
}

/* Task b: print the game field
 */
void print_field(void) {
  for (unsigned int y = 0; y < ysize; y++) {
    for (unsigned int x = 0; x < xsize; x++) {
      printf("%c", get_char_to_print(x, y));
    }
    printf("\n");
  }
}

int is_within_bounds(int i, int max) {
  return i >= 0 && i < max;
}

/* Task c: Ask coordinates (two integers) from user, and shoot the location.
 * Returns -1 if user gave invalid input or coordinates, 0 if there was no ship
 * at the given location; and 1 if there was a ship hit at the location.
 */
int shoot(void) {
  int x, y, got_nums;
  got_nums = scanf("%d %d", &x, &y);
  if ((got_nums != 2) || (!is_within_bounds(x, xsize) || (!is_within_bounds(y, ysize)))) {
    return -1;
  }

  checked(x, y);

  if (is_ship(x, y) == '+') {
    hit_ship(x, y);
    return 1;
  } else {
    return 0;
  }
}

/* Task d: Returns 1 if game is over (all ships are sunk), or 0 if there
 * still are locations that have not yet been hit. <num> is the number of
 * ships on the game map. It is assumed to be the same as in the call to
 * set_ships function.
 */
int game_over(unsigned int num) {
  int res = 1;

  for (unsigned int x = 0; x < xsize; x++) {
    for (unsigned int y = 0; y < ysize; y++) {
      if (is_ship(x, y) == '+') {
        res = 0;
        break;
      }
    }
  }

  return res;
}
