#include "source.h"
#include <math.h>
#include <stdio.h>

void simple_sum(void) {
  int num1, num2;
  scanf("%d %d", &num1, &num2);
  printf("%d + %d = %d\n", num1, num2, num1 + num2);
}

void simple_math(void) {
  float num1, num2;
  char op;
  scanf("%f %c %f", &num1, &op, &num2);
  switch (op) {
    case '+':
      printf("%.1f\n", num1 + num2);
      break;
    case '-':
      printf("%.1f\n", num1 - num2);
      break;
    case '*':
      printf("%.1f\n", num1 * num2);
      break;
    case '/':
      printf("%.1f\n", num1 / num2);
      break;
    default:
      printf("ERR\n");
  }
}
