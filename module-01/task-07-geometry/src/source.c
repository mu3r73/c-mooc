#include "source.h"
#include <math.h>
#include <stdio.h>

void multi_table(unsigned int xsize, unsigned int ysize) {
  for (unsigned int y = 1; y <= ysize; y++) {
    for (unsigned int x = 1; x <= xsize; x++) {
      printf("% 4d", x * y);
    }
    printf("\n");
  }
}

void draw_triangle(unsigned int size) {
  for (unsigned int y = 0; y < size; y++) {
    for (unsigned int x = 0; x < size; x++) {
      if (x < size - y - 1) {
        printf("%s", ".");
      } else {
        printf("%s", "#");
      }
    }
    printf("\n");
  }
}

double distance(int x, int y) {
  return sqrt(x * x + y * y);
}

void draw_ball(unsigned int radius) {
  unsigned int size = 2 * radius + 1;
  for (unsigned int y = 0; y < size; y++) {
    for (unsigned int x = 0; x < size; x++) {
      if (distance(x - radius, y - radius) <= radius) {
        printf("%s", "*");
      } else {
        printf("%s", ".");
      }
    }
    printf("\n");
  }
}
