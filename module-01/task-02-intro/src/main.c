#include "source.h"
#include <stdio.h>

int main() {
  printf("\n--- Three lines ---\n");
  three_lines();

  printf("\n--- Fix types ---\n");
  fix_types();

  return 0;
}
