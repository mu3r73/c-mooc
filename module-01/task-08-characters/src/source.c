#include "source.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>

/* Don't touch the definition of msgs array! Checker uses this. */
char *msgs[10] = {
  "'6=*w+~)._", "J65+~5+~=0/*69,~+9;,9*~19++=79"
};

char get_printable(int c) {
  if (isprint(c)) {
    return c;
  } else {
    return '?';
  }
}

char get_separator(int q) {
  if (q % 4 == 0) {
    return '\n';
  } else {
    return '\t';
  }
}

void ascii_chart(char min, char max) {
  int q = 1;
  for (char i = min; i <= max; i++) {
    printf("% 3d 0x%2x %c%c", i, i, get_printable(i), get_separator(q));
    q++;
  }
}

char get_character(int msg, unsigned int cc) {
  if (msg >= 10 || !msgs[msg])
    return 0;

  if (strlen(msgs[msg]) <= cc)
    return 0;

  return msgs[msg][cc];
}

char decrypt_char(char c) {
  return 158 - c;
}

void secret_msg(int msg) {
  char c;
  unsigned int cc = 0;
  c = get_character(msg, cc);
  while (c != 0) {
    printf("%c", decrypt_char(c));
    cc++;
    c = get_character(msg, cc);
  }
}
