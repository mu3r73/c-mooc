#include "election.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void set_name(struct votes *v, char *name) {
  strcpy(v->name, name);
}

void set_votes(struct votes *v, int num) {
  v->votes = num;
}

void init_record(struct votes *v) {
  set_name(v, (char *)"");
  set_votes(v, 0);
}

struct votes *create_votes() {
  struct votes *res = (struct votes *)malloc(sizeof(struct votes));
  if (res == NULL) {
    return NULL;
  }
  init_record(res);
  return res;
}

struct votes *add_record(struct votes *array, int len) {
  struct votes *res = (struct votes *)realloc(array, sizeof(struct votes) * (len + 1));
  if (res == NULL) {
    return NULL;
  }
  init_record(&res[len]);
  // free(array);
  return res;
}

int find_pos(struct votes *lv, char *name) {
  int pos = -1;

  for (size_t i = 0; ; i++) {
    if (strcmp(lv[i].name, "") == 0) {
      break;
    }
    if (strcmp(lv[i].name, name) == 0) {
      pos = i;
    }
  }

  return pos;
}

struct votes *read_votes(const char *filename) {
  FILE *f = fopen(filename, "r");
  if (f == NULL) {
    return NULL;
  }

  struct votes *res = create_votes();
  if (res == NULL) {
    return NULL;
  }

  char name[40] = "";
  int len = 0;
  int idx = 0;

  int c = fgetc(f);
  while (c != EOF) {
    if ((c == '\n') && (len > 0)) {
      name[len] = '\0';
      int pos = find_pos(res, name);
      if (pos > -1) {
        res[pos].votes++;
      } else {
        res = add_record(res, idx + 1);
        if (res == NULL) {
          return NULL;
        }
        set_name(&res[idx], name);
        set_votes(&res[idx], 1);
        idx++;
      }
      len = 0;
    } else {
      name[len++] = c;
    }
    c = fgetc(f);
  }

  fclose(f);
  return res;
}

unsigned int get_len(struct votes *v) {
  unsigned int res = 0;
  for (size_t i = 0; ; i++) {
    if (strcmp(v[i].name, "") == 0) {
      break;
    }
    res++;
  }
  return res;
}

int votes_n_name_cmp(const void *a, const void *b) {
  const struct votes *va = (struct votes *)a;
  const struct votes *vb = (struct votes *)b;

  int res = 0;
  if (va->votes > vb->votes) {
    res = -1;
  } else if (va->votes < vb->votes) {
    res = 1;
  }

  if (res != 0) {
    return res;
  } else {
    return strcmp(va->name, vb->name);
  }
}

void sort_array(struct votes *v, unsigned int len) {
  qsort(v, len, sizeof(struct votes), votes_n_name_cmp);
}

void results(struct votes *vlist) {
  unsigned int len = get_len(vlist);
  sort_array(vlist, len);
  for (size_t i = 0; i < len; i++) {
    printf("%s: %d\n", vlist[i].name, vlist[i].votes);
  }
}
