#include "getopt.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* a) Returns the list of options given on command line arguments
 * indicated in <argc> and <argv>. These parameters are used similarly as
 * in the main function.
 *
 * Function returns pointer to the start of linked list that contains the
 * options.
 */
int is_opt(char *s) {
  if (s[0] == '-') {
    return 1;
  } else {
    return 0;
  }
}

char get_opt(char *s) {
  return s[1];
}

struct options *new_struct() {
  struct options *res;
  res = (struct options *)malloc(sizeof(struct options));
  res->optchar = '-';
  res->optstring = NULL;
  res->next = NULL;
  return res;
}

int is_blank(char *s) {
  int res = 1;
  for (size_t i = 0; ; i++) {
    if (s[i] == '\0') {
      break;
    }
    if (s[i] != ' ') {
      res = 0;
      break;
    }
  }
  return res;
}

struct options *get_options(int argc, char *argv[]) {
  struct options *res = new_struct();

  struct options *curr = res;
  struct options *prev;
  int first_opt = 1;
  for (int i = 0; i < argc; i++) {
    if (is_opt(argv[i])) {
      if (first_opt == 0) {
        prev = curr;
        curr = new_struct();
        prev->next = curr;
      }
      curr->optchar = get_opt(argv[i]);
      first_opt = 0;
    } else {
      if ((curr->optchar != '-') && (is_blank(argv[i]) == 0)) {
        size_t len = strlen(argv[i]);
        curr->optstring = (char *)malloc(sizeof(char) * (len + 1));
        strncpy(curr->optstring, argv[i], len + 1);
      }
    }
  }

  return res;
}

/* a) Free the memory allocated for the linked list at <opt>.
 */
void free_options(struct options *opt) {
  struct options *aux = opt;
  while (aux != NULL) {
    struct options *prev = aux;
    aux = aux->next;
    free(prev);
  }
}

/* b) Returns non-zero if option character <optc> is included in the
 * linked list pointed by <opt>
 */
int is_option(struct options *opt, char optc) {
  int res = 0;
  struct options *aux = opt;

  while (aux != NULL) {
    if (aux->optchar == optc) {
      res = 1;
      break;
    }
    aux = aux->next;
  }

  return res;
}

/* b) Returns the argument given with option <optc>. If argument or
 * the option was not given, the function returns NULL.
 */
char *get_optarg(struct options *opt, char optc) {
  char *res = NULL;
  struct options *aux = opt;

  while (aux != NULL) {
    if (aux->optchar == optc) {
      res = aux->optstring;
      break;
    }
    aux = aux->next;
  }

  return res;
}
