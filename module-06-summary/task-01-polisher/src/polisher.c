#include "polisher.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Read given file <filename> to dynamically allocated memory.
 * Return pointer to the allocated memory with file content, or
 * NULL on errors.
 */
char *read_file(const char *filename) {
  FILE *f = fopen(filename, "r");
  if (f == NULL) {
    return NULL;
  }

  fseek(f, 0, SEEK_END); // seek to end of file
  long size = ftell(f); // get current file pointer
  fseek(f, 0, SEEK_SET); // seek back to beginning of file

  char *res = (char *)malloc(sizeof(char) * size);
  fread(res, sizeof(char), size, f);
  if (ferror(f)) {
    free(res);
    return NULL;
  }

  fclose(f);
  return res;
}

/* Remove C comments from the program stored in memory block <input>.
 * Returns pointer to code after removal of comments.
 * Calling code is responsible of freeing only the memory block returned by
 * the function.
 */
char *remove_comments(char *input) {
  size_t len = strlen(input);
  char *output = (char *)malloc(sizeof(char) * (len + 1));
  if (output == NULL) {
    return input;
  }

  char found_slash = 0;
  char inside_line_comment = 0;
  char inside_multiline_comment = 0;
  char found_star = 0;
  size_t written = 0;

  for (size_t i = 0; i < len; i++) {
    switch (input[i]) {
      case '\n':
      if (inside_multiline_comment == 1) {
          continue;
        } else if (inside_line_comment == 1) {
          inside_line_comment = 0;
        } else {
          output[written++] = input[i];
        }
        break;
      case '/':
        if (found_star == 1) {
          inside_multiline_comment = 0;
          found_star = 0;
        } else if (found_slash == 0) {
          found_slash = 1;
        } else if (found_slash == 1) {
          inside_line_comment = 1;
          found_slash = 0;
        }
        break;
      case '*':
        if (found_slash == 1) {
          inside_multiline_comment = 1;
          found_slash = 0;
        } else if (inside_multiline_comment == 1) {
          found_star = 1;
        }
        break;
      default:
        if (found_slash == 1) {
          output[written++] = '/';
          found_slash = 0;
        } else if (found_star == 1) {
          output[written++] = '*';
          found_star = 0;
        } else if ((inside_line_comment == 1) || (inside_multiline_comment == 1)) {
          continue;
        }
        output[written++] = input[i];
    }
  }
  output[written] = '\0';

  free(input);
  return output;
}

/* Indent the C-code at memory block <indent>. String <pad> represents
 * one block of indentation. Only opening curly braces '{' increase the
 * indentation level, and closing curly braces '}' decrease the indentation
 * level.
 * Return the pointer to the code after modification.
 * Calling code is responsible of freeing only the memory block returned by
 * the function.
 */
char *indent(char *input, const char *pad) {
  size_t len = strlen(input);
  char *output = (char *)malloc(sizeof(char) * (len * 4));
  if (output == NULL) {
    return input;
  }

  size_t indent_len = strlen(pad);
  unsigned char indents = 0;
  size_t written = 0;

  for (size_t i = 0; i < len; i++) {
    switch (input[i]) {
      case '{':
        indents++;
        output[written++] = input[i];
        break;
      case '}':
        indents--;
        written -= indent_len;
        output[written++] = input[i];
        break;
      case '\n':
        output[written++] = input[i];
        for (size_t j = 0; j < indents; j++) {
          for (size_t k = 0; k < indent_len; k++) {
            output[written++] = pad[k];
          }
        }
        break;
      default:
        output[written++] = input[i];
    }
  }

  output[written] = '\0';

  // free(input);
  return output;
}
