/* monster.c -- Implementation of monster actions
 */

#include "dungeon.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// for defining some monster types below that can be used in the game
typedef struct {
  char name[20];       // Name of monster
  char sign;           // character to show it on map
  unsigned int hplow;  // lowest possible initial maxhp
  unsigned int hphigh; // highest possible initial maxhp
} MonstType;

// Specifying three monster types to start with.
// Feel free to add more, or change the below
// Note that it is up to you to decide whether to use this array from
// createMonsters
// you may or may not use it
const int numtypes = 3;
const MonstType types[] = {
  {"Goblin", 'G', 6, 10},
  {"Rat", 'R', 3, 5},
  {"Dragon", 'D', 15, 20}
};

/* One kind of attack done by monster.
 * The attack function pointer can refer to this.
 *
 * Parameters:
 * game: the game state
 * monst: The monster performing attack
 */
void attackPunch(Game *game, Creature *monst) {
  printf("%s punches you! ", monst->name);
  int hitprob = 50;
  int maxdam = 4;
  if (rand() % 100 < hitprob) {
    printf("Hit! ");
    int dam = rand() % maxdam + 1;
    printf("Damage: %d ", dam);
    game->hp = game->hp - dam;
    if (game->hp <= 0)
      printf("You died!");
    printf("\n");
  } else {
    printf("Miss!\n");
  }
}

/* Exercise (c)
 *
 * Move monster 'monst' towards the player character.
 * See exercise description for more detailed rules.
 */
double distance(Point p1, Point p2) {
  return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

void moveTowards(Game *game, Creature *monst) {
  Point p;
  double min_dist = distance(game->position, monst->pos);
  for (int x = monst->pos.x - 1; x <= monst->pos.x + 1; x++) {
    for (int y = monst->pos.y - 1; y <= monst->pos.y + 1; y++) {
      if ((isBlocked(game, x, y) == 1)
          || ((x == monst->pos.x) && (y == monst->pos.y))
          || ((x == game->position.x) && (y == game->position.y))) {
        continue;
      }
      Point paux = {x, y};
      double dist = distance(game->position, paux);
      if (dist < min_dist) {
        p = paux;
        min_dist = dist;
      }
    }
  }
  monst->pos = p;
}

/* Exercise (d)
 *
 * Move monster 'monst' away from the player character.
 * See exercise description for more detailed rules.
 */
void moveAway(Game *game, Creature *monst) {
  Point p;
  double max_dist = distance(game->position, monst->pos);
  for (int x = monst->pos.x - 1; x <= monst->pos.x + 1; x++) {
    for (int y = monst->pos.y - 1; y <= monst->pos.y + 1; y++) {
      if ((isBlocked(game, x, y) == 1)
          || ((x == monst->pos.x) && (y == monst->pos.y))
          || ((x == game->position.x) && (y == game->position.y))) {
        continue;
      }
      Point paux = {x, y};
      double dist = distance(game->position, paux);
      if (dist > max_dist) {
        p = paux;
        max_dist = dist;
      }
    }
  }
  monst->pos = p;
}

/* Exercise (e)
 *
 * Take action on each monster (that is alive) in 'monsters' array.
 * Each monster either attacks or moves (or does nothing if no action is
 * specified)
 */
void monsterAction(Game *game) {
  for (size_t i = 0; i < game->numMonsters; i++) {
    if (game->monsters[i].hp == 0) {
      continue;
    }
    if (distance(game->monsters[i].pos, game->position) <= sqrt(2)) {
      game->monsters[i].attack(game, &game->monsters[i]);
    } else {
      game->monsters[i].move(game, &game->monsters[i]);
    }
  }
}

/* Exercise (b)
 *
 * Create opts.numMonsters monsters and position them on valid position
 * in the the game map. The moster data (hitpoints, name, map sign) should be
 * set appropriately (see exercise instructions)
 */
Point get_random_non_blocked_pos(Game *game) {
  Point p;
  do {
    p.x = rand() % game->opts.mapWidth;
    p.y = rand() % game->opts.mapHeight;
  } while (isBlocked(game, p.x, p.y) == 1);
  return p;
}

void createMonster(Game *game, size_t idx) {
  size_t rnd_idx = rand() % numtypes;
  strcpy(game->monsters[idx].name, types[rnd_idx].name);
  game->monsters[idx].sign = types[rnd_idx].sign;
  game->monsters[idx].maxhp = types[rnd_idx].hphigh;
  game->monsters[idx].hp = game->monsters[idx].maxhp;
  game->monsters[idx].pos = get_random_non_blocked_pos(game);
  game->monsters[idx].move = moveTowards;
  game->monsters[idx].attack = attackPunch;
}

void createMonsters(Game *game) {
  game->monsters = (Creature *)malloc(sizeof(Creature) * game->opts.numMonsters);
  if (game->monsters == NULL) {
    return;
  }

  for (size_t i = 0; i < game->opts.numMonsters; i++) {
    createMonster(game, i);
    game->numMonsters++;
  }
}

// typedef struct creature_st {
//   Point pos;          // location of the monster
//   void (*move)(struct game_st *,
//                struct creature_st *); // current movement algorithm for monster
//   void (*attack)(struct game_st *,
//                  struct creature_st *); // current attack algorithm for monster
// } Creature;


/* Determine whether monster moves towards or away from player character.
 */
void checkIntent(Game *game) {
  for (unsigned int i = 0; i < game->numMonsters; i++) {
    Creature *m = &game->monsters[i];
    if (m->hp <= 2) {
      m->move = moveAway;
    } else {
      m->move = moveTowards;
    }
    if (m->hp < m->maxhp)
      m->hp = m->hp + 0.1; // heals a bit every turn
  }
}
