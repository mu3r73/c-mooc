#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int myprint(const char *s, ...) {
  int count = 0;

  va_list args;
  va_start(args, s);

  for (unsigned int i = 0; ; i++) {
    if (s[i] == '\0') {
      break;
    }
    if (s[i] == '&') {
      printf("%d", va_arg(args, int));
      count++;
    } else {
      putchar(s[i]);
    }
  }

  va_end(args);

  return count;
}
