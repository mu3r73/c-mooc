#include "base64.h"
#include <stdio.h>
#include <stdlib.h>

/* The set of base64-encoded characters. You may use this table if you want.
 * (The last character is not part of the actual base64 set, but used for
 * padding).
 */
const char *encoding =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

/* Open file named <src_file> for reading, and convert it to Base64 format,
 * which is written to file named <dst_file>.
 *
 * Returns: number of bytes in <src_file>, or -1 if there was an error,
 * for example if opening of <src_file> did not succeed.
 */
void from3to4(const char *bin, int read, char *bout) {
  for (unsigned int i = 0; i < 4; i++) {
    bout[i] = '=';
  }
  if (read > 0) {
    bout[0] = encoding[bin[0] >> 2];
  }
  if (read > 1) {
    bout[1] = encoding[((bin[0] & 0x03) << 4) + (bin[1] >> 4)];
    if (read == 2) {
      bout[2] = encoding[(bin[1] & 0x0F) << 2];
    }
  }
  if (read > 2) {
    bout[2] = encoding[((bin[1] & 0x0F) << 2) + (bin[2] >> 6)];
    bout[3] = encoding[bin[2] & 0x3F];
  }
}

int to_base64(const char *dst_file, const char *src_file) {
  FILE *fin = fopen(src_file, "r");
  if (fin == NULL) {
    return -1;
  }
  FILE *fout = fopen(dst_file, "w");
  if (fout == NULL) {
    return -1;
  }

  char bin[3];
  char bout[4];
  int read;
  int count = 0;
  while (!feof(fin)) {
    read = fread(bin, sizeof(char), 3, fin);
    if (ferror(fin)) {
      return -1;
    }

    if (read > 0) {
      from3to4(bin, read, bout);

      fwrite(bout, sizeof(char), 4, fout);
      if (ferror(fout)) {
        return -1;
      }
      count += read;
      if ((count * 4 / 3) % 64 == 0) {
        fputc('\n', fout);
      }
    }
  }

  fclose(fin);
  fclose(fout);
  return count;
}

/* Open Base64-encoded file named <src_file> for reading, and convert it
 * to regular binary format, which is written to file named <dst_file>.
 *
 * Returns: number of bytes in <src_file>, or -1 if there was an error,
 * for example if opening of <src_file> did not succeed.
 */
unsigned char get_index(char c) {
  unsigned char res = 0;
  while (encoding[res] != c) {
    res++;
  }
  return res;
}

int from4to3(const char *bin, char *bout) {
  bout[0] = get_index(bin[0]) << 2;
  if (bin[1] == '=') {
    return 1;
  }
  bout[0] += get_index(bin[1]) >> 4;

  bout[1] = get_index(bin[1]) << 4;
  if (bin[2] == '=') {
    if (bout[1] == 0) {
      return 1;
    } else {
      return 2;
    }
  }
  bout[1] += get_index(bin[2]) >> 2;

  bout[2] = get_index(bin[2]) << 6;
  if (bin[3] == '=') {
    if (bout[2] == 0) {
      return 2;
    } else {
      return 3;
    }
  }
  bout[2] += get_index(bin[3]);
  return 3;
}

int from_base64(const char *dst_file, const char *src_file) {
  FILE *fin = fopen(src_file, "r");
  if (fin == NULL) {
    return -1;
  }
  FILE *fout = fopen(dst_file, "w");
  if (fout == NULL) {
    return -1;
  }

  char bin[4];
  char bout[3];
  int read, conv;
  int count = 0;
  int count_crlfs = 0;
  while (!feof(fin)) {
    read = fread(bin, sizeof(char), 4, fin);
    if (ferror(fin)) {
      return -1;
    }

    if (read > 0) {
      conv = from4to3(bin, bout);
      fwrite(bout, sizeof(char), conv, fout);
      if (ferror(fout)) {
        return -1;
      }
      count += read;
      if (count % 64 == 0) {
        count_crlfs++;
        fgetc(fin); // discarding '\n's
      }
    }
  }
  fclose(fin);
  fclose(fout);
  return count + count_crlfs;
}
