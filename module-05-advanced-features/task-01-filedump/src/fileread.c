#include "fileread.h"
#include <ctype.h>
#include <stdio.h>

/* Prints the given file as text on the screen.
 * Only printable characters are shown. Non-printable characters are printed
 * as '?'. <filename> parameter is the name of the file.
 *
 * Returns the number of characters read
 */
int textdump(const char *filename) {
  FILE *f = fopen(filename, "r");
  if (f == NULL) {
    return -1;
  }

  int count = 0;
  int c = fgetc(f);
  while (c != EOF) {
    count++;
    if (!isprint(c)) {
      c = '?';
    }
    fputc(c, stdout);
    c = fgetc(f);
  }
  return count;
}

/* Prints the given file as hexdump, at most 16 numbers per line.
 * <filename> parameter is the name of the file.
 *
 * Returns the number of characters read
 */
int hexdump(const char *filename) {
  FILE *f = fopen(filename, "r");
  if (f == NULL) {
    return -1;
  }

  int count = 0;
  int c = fgetc(f);
  while (c != EOF) {
    count++;
    printf("%.2x ", c & 0x00FF);
    if (count % 16 == 0) {
      printf("\n");
    }
    c = fgetc(f);
  }
  return count;
}
