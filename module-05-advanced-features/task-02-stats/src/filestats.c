#include "filestats.h"
#include <ctype.h>
#include <stdio.h>

/* Returns the line count in given file
 *
 * Parameters:
 * filename: name of the file to be investigated.
 *
 * Returns:
 * The number of lines in file. */
int line_count(const char *filename) {
  FILE *f = fopen(filename, "r");
  if (f == NULL) {
    return -1;
  }

  int count = 0;
  int chars_before_eol = 0;
  int c = fgetc(f);
  while (c != EOF) {
    chars_before_eol++;
    if (c == '\n') {
      count++;
      chars_before_eol = 0;
    }
    c = fgetc(f);
  }
  if (chars_before_eol > 0) {
    count++;
  }
  return count;
}

/* Count the number of words in the file. Word has to include at least one
 * alphabetic character, and words are separated by whitespace.
 *
 * Parameters:
 * filename: name of the file to be investigated.
 *
 * Returns:
 * number of words in the file */
int word_count(const char *filename) {
  FILE *f = fopen(filename, "r");
  if (f == NULL) {
    return -1;
  }

  int count = 0;
  int alpha_chars_in_word = 0;
  int c = fgetc(f);
  while (c != EOF) {
    if (isalpha(c)) {
      alpha_chars_in_word++;
    }
    if ((c == ' ') || (c == '\n')) {
      if (alpha_chars_in_word > 0) {
        count++;
      }
      alpha_chars_in_word = 0;
    }
    c = fgetc(f);
  }
  if (alpha_chars_in_word > 0) {
    count++;
  }
  return count;
}
